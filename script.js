/*Деструктуризація - це поділ складної структури (об'єкту чи масива) на прості частини.
Використовуючи синтаксис деструктуризації можливо отримати менші фрагменти з масивів і
об'єктів. Синтаксис десктруктуризації можна використовувати для оголошення або
присвоєння змінної. Також можна обробляти вкладені структури за допомогою
синтаксису деструктуризації.
 */
'use strict'

// ЗАВДАННЯ 1
/*
const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const baseUnited = [...clients1, ...clients2]
const newBase = new Set(baseUnited);
const data = Array.from(newBase);
console.log(data)

 */

// ЗАВДАННЯ 2
/*
const characters = [
    {
        name: "Елена",
        lastName: "Гилберт",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Кэролайн",
        lastName: "Форбс",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Аларик",
        lastName: "Зальцман",
        age: 31,
        gender: "man",
        status: "human"
    },
    {
        name: "Дэймон",
        lastName: "Сальваторе",
        age: 156,
        gender: "man",
        status: "vampire"
    },
    {
        name: "Ребекка",
        lastName: "Майклсон",
        age: 1089,
        gender: "woman",
        status: "vampire"
    },
    {
        name: "Клаус",
        lastName: "Майклсон",
        age: 1093,
        gender: "man",
        status: "vampire"
    }
];

const {name, lastName, age, ...rest} = characters;
console.log(characters);

const charactersShortInfo = [];

characters.forEach((e) => {
    const {name, lastName, age} = e;

    charactersShortInfo.push({
        name: name,
        lastName: lastName,
        age: age,
    })
})

console.log(charactersShortInfo);

 */

// ЗАВДАННЯ 3
/*
const user1 = {
    name: "John",
    years: 30
};

const {
    name: name,
    years: age,
    isAdmin: isAdmin = false,
} = user1;

console.log(`Name: ${name}, years ${age}, isAdmin ${isAdmin}`);
 */

// ЗАВДАННЯ 4
/*
const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
        lat: 38.869422,
        lng: 139.876632
    }
}

const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
}

const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto',
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
}

const dataBaseSatoshi = [{...satoshi2018}, {...satoshi2019}, {...satoshi2020}]

const fullProfile = {};
for (let item of dataBaseSatoshi) {
    // console.log(item);
    for(let field in item) {
        // console.log(`field name ${field} value ${item[field]}`);
        fullProfile[field] = item[field];
    }
}

console.log(fullProfile)
 */

// ЗАВДАННЯ 5
/*
const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
}, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
}, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
}

const updatedBooks = [...books,bookToAdd]
console.log(updatedBooks)
 */

// ЗАВДАННЯ 6
/*
const employee = {
    name: 'Vitalii',
    surname: 'Klichko',
}
const add = { age: 45, salary: 5000}
const NewObject = {...employee, ...add}
console.log(NewObject)
 */

// ЗАВДАННЯ 7

const array = ['value', () => 'showValue'];
const value = array[0];
const showValue = array[1];

alert(value);
alert(showValue());
